package anonymizer

import (
	"errors"
	"fmt"
	"regexp"
	"strings"
)

// Simple usage:
// pa := NewPhoneAnonymizer()
// pa.Anonymize("+48 666 777 888") -> +48 666 777 XXX
// pa.Replacement = "."
// pa.SetLastDigits(5)
// pa.Anonymize("+48 666 777 888") -> +48 666 7.. ...

type phoneAnonymizer struct {
	replacement rune
	lastDigits  int
}

func NewPhoneAnonymizer() *phoneAnonymizer {
	return &phoneAnonymizer{
		replacement: 'X',
		lastDigits:  3,
	}
}

func (pa *phoneAnonymizer) SetLastDigits(num int) error {
	if num < 0 || num > 9 {
		return errors.New("phoneAnonymizer: lastDigits must be >= 0 and <= 9")
	}

	pa.lastDigits = num
	return nil
}

func (pa *phoneAnonymizer) SetReplacement(newReplacement string) *phoneAnonymizer {
	pa.replacement = []rune(newReplacement)[0]
	return pa
}

func (pa *phoneAnonymizer) Anonymize(s string) string {
	regexPhone := regexp.MustCompile(`(\d{3}\s\d{3}\s\d{3})`)
	arrayPhone := regexPhone.FindAllString(s, -1)
	if len(arrayPhone) == 0 {
		return s
	}

	for _, v := range arrayPhone {
		startIndex := len(v) - pa.lastDigits
		numReplaceCharacter := strings.Repeat(string(pa.replacement), pa.lastDigits)
		beforeStr := v[0:startIndex]
		str := fmt.Sprintf("%s%s", beforeStr, numReplaceCharacter)
		s = strings.Replace(s, v, str, -1)
	}

	return s
}
