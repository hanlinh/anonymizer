package anonymizer

import (
	"testing"

	a "gitlab.com/hanlinhqn/anonymizer/anonymizer"
)

var S_TEST_DATA = []struct {
	orig     string
	expected string
}{
	{`Lorem ipsum`, `Lorem ipsum`},
	{`Lorem ipsum <a href="skype:loremipsum?call">call</a> dolor sit amet`, `Lorem ipsum <a href="skype:#?call">call</a> dolor sit amet`},
	{`Lorem ipsum  <a href="skype:loremipsum?call">call</a>, dolor sit <a href="skype:IPSUMLOREM?chat">chat</a> amet`, `Lorem ipsum  <a href="skype:#?call">call</a>, dolor sit <a href="skype:#?chat">chat</a> amet`},
}

func TestSkypeAnonymize(t *testing.T) {
	sa := a.NewSkypeAnonymizer("#")
	for _, value := range S_TEST_DATA {
		anonymized := sa.Anonymize(value.orig)
		if anonymized != value.expected {
			t.Errorf("Anonymize(%s): expected %s, actual %s", value.orig, value.expected, anonymized)
		}
	}
}
