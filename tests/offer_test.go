package anonymizer

import (
	"testing"

	a "gitlab.com/hanlinhqn/anonymizer/anonymizer"
)

var O_TEST_DATA = []struct {
	orig     string
	expected string
}{
	{`Lorem ipsum a@a.com. <a href="skype:loremipsum?call">call</a> +48 666 777 888`,
		`Lorem ipsum REPLACED@a.com. <a href="skype:REPLACED?call">call</a> +48 666 777 8XX`},
}

var O_TEST_ANONYMIZERS = []a.Anonymizer{
	a.NewEmailAnonymizer("REPLACED"),
	a.NewSkypeAnonymizer("REPLACED"),
	getPhoneAnonymizer(),
}

func getPhoneAnonymizer() a.Anonymizer {
	pa := a.NewPhoneAnonymizer().SetReplacement("XXX")
	pa.SetLastDigits(2)
	return pa
}

func TestOfferAnonymize(t *testing.T) {
	oa := a.NewOfferAnonymizer()

	for _, value := range O_TEST_ANONYMIZERS {
		oa.AddAnonymizer(value)
	}

	for _, value := range O_TEST_DATA {
		anonymized := oa.Anonymize(value.orig)
		if anonymized != value.expected {
			t.Errorf("Anonymize(%s): expected %s, actual %s", value.orig, value.expected, anonymized)
		}
	}
}
