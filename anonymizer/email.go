package anonymizer

import (
	"fmt"
	"regexp"
	"strings"
)

// Simple usage:
// ea := NewEmailAnonymizer("***").Anonymize("a-a@a.b.c") -> ***@a.b.c
// ea.Replacement = "..."
// ea.Anonymize("a-a@a.b.c") -> ...@a.b.c

type emailAnonymizer struct {
	Replacement string
}

func NewEmailAnonymizer(replacement string) *emailAnonymizer {
	return &emailAnonymizer{
		Replacement: replacement,
	}
}

func (ea *emailAnonymizer) Anonymize(s string) string {
	regexEmail := regexp.MustCompile(`([a-zA-Z0-9._-]+@[a-zA-Z0-9._-]+\.[a-zA-Z0-9_]+)`)
	regexEmailPrefix := regexp.MustCompile(`(([a-zA-Z0-9._]+[a-zA-Z0-9._-]+@)|([a-zA-Z0-9._]+@))`)

	arrayEmail := regexEmail.FindAllString(s, -1)
	if len(arrayEmail) == 0 {
		return s
	}

	for _, v := range arrayEmail {
		emailPrefix := regexEmailPrefix.FindString(v)
		newSymbol := fmt.Sprintf("%s@", ea.Replacement)
		s = strings.Replace(s, emailPrefix, newSymbol, -1)
	}

	return s
}
