package anonymizer

import (
	"testing"

	a "gitlab.com/hanlinhqn/anonymizer/anonymizer"
)

var P_TEST_DATA = []struct {
	replacement string
	last_digits int
	original    string
	expected    string
}{
	{"X", 3, "Lorem ipsum", "Lorem ipsum"},
	{"X", 0, "Lorem +48 666 666 666 d00r", "Lorem +48 666 666 666 d00r"},
	{"X", 3, "Lorem +48 666 666 666 d11r", "Lorem +48 666 666 XXX d11r"},
	{"*", 3, "Lorem +48 666 666 666, +48 777 777 777 sit 888 888 888 amet", "Lorem +48 666 666 ***, +48 777 777 *** sit 888 888 *** amet"},
}

func TestPhoneAnonymize(t *testing.T) {
	for _, value := range P_TEST_DATA {
		pa := a.NewPhoneAnonymizer()
		pa.SetReplacement(value.replacement)
		pa.SetLastDigits(value.last_digits)
		anonymized := pa.Anonymize(value.original)
		if anonymized != value.expected {
			t.Errorf("Anonymize(%s): expected %s, actual %s", value.original, value.expected, anonymized)
		}
	}
}
