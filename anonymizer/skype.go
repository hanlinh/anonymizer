package anonymizer

import (
	"fmt"
	"regexp"
	"strings"
)

// Simple usage:
// ea := NewSkypeAnonymizer("#").Anonymize(`<a href="skype:loren?call"/>`) -> <a href="skype:#?call"/>

type skypeAnonymizer struct {
	Replacement string
}

func NewSkypeAnonymizer(replacement string) *skypeAnonymizer {
	return &skypeAnonymizer{
		Replacement: replacement,
	}
}

func (sa *skypeAnonymizer) Anonymize(s string) string {
	skypeRegex := regexp.MustCompile(`skype:([a-zA-Z]+)`)
	arrayName := skypeRegex.FindAllString(s, -1)
	if len(arrayName) == 0 {
		return s
	}

	for _, v := range arrayName {
		newStr := fmt.Sprintf("skype:%s", sa.Replacement)
		s = strings.Replace(s, v, newStr, -1)
	}

	return s
}
